<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <link href="formTicket.css" rel="stylesheet" type="text/css"> 
    </head>

    <?php
        $login = $_POST["login"];
    ?>


    <body>
        <header>
            <img class="logo" src="logo.png">
            <img class="sli" src="sli.png">
        </header>

        <div class="container">
            <p class="state green"> connected </P>
            <form action="recupTicket.php" method="post" >
                <h1>REQUEST</h1>
                <fieldset class="priority">
                    <label for="prio">priority:</label>
                    <div class="priority__cont">
                        <input type="radio" class="rad" data-num="0" name="prio" value="faible">
                        <input type="radio" class="rad" data-num="1" name="prio" value="modere">
                        <input type="radio" class="rad" data-num="2" name="prio" value="moyen">
                        <input type="radio" class="rad" data-num="3" name="prio" value="grand">
                        <input type="radio" class="rad" data-num="4" name="prio" value="urgent">
                    </div>
                </fieldset>
                <fieldset class="subject">
                    <label for="subject">subject:</label>
                    <input class="subject__input line-input" type="text" name="subject" >
                </fieldset>
                <fieldset class="description">
                    <label for="description"> describe your problem:</label>
                    <textarea class="description__textarea" name="description"></textarea>
                </fieldset>
                <fieldset class="sector">
                    <p>sector:</P>
                    <input class="sector__input line-input" type="text" name="sector" >
                </fieldset>
                <input type="hidden" name="login" value="<?php echo $login ?>" >
                <div class="bottom">
                    <button class="send" type="submit">Send</button>
                    <?php if($login=="root"):?>
                        <a class="ticket" href="http://localhost/zooticoon/afficheListeTickets/afficheListeTickets.html">
                        see tickets
                        </a>
                    <?php endif; ?>
                </div>

            </form>
        </div>
    </body>