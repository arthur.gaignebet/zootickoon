    const rad = document.querySelectorAll(".rad")

rad.forEach((element) => {
    element.addEventListener('click', (ev) => {
        let num = ev.target.dataset.num;
        for (let i=0; i<num; ++i) {
            rad[i].classList.add("checked")
        }
        for (let i=num; i<rad.length; ++i) {
            rad[i].classList.remove("checked")
        }
    })
})

