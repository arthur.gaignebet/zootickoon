const hamburger = document.querySelector(".side__hamburger") 
const arrow = document.querySelector(".side__arrow")
const menu = document.querySelector(".menu")
const anchor = document.querySelectorAll(".anchor__li")

function toggleMenu () {
    hamburger.classList.toggle("active")
    arrow.classList.toggle("active")
    menu.classList.toggle("active")
}

hamburger.addEventListener('click', toggleMenu)
arrow.addEventListener('click', toggleMenu)
anchor.forEach((el) => {
    el.addEventListener('click', toggleMenu)
})

// Resize
const liList1 = document.querySelectorAll('.a_li');
const liList2 = document.querySelectorAll('.b_li');
const stateDiv1 = document.querySelector('.slideshow1__logic__state');
const stateDiv2 = document.querySelector('.slideshow2__logic__state');
console.log(stateDiv2)
function resize () {
    let stateWidth1 = stateDiv1.offsetWidth;
    let liWidth1 = liList1[0].offsetWidth;
    let newSize1 = (stateWidth1 - 3*liWidth1)/2 
    stateDiv1.style.setProperty('--width', `${newSize1}px`)
    let stateWidth2 = stateDiv2.offsetWidth;
    let liWidth2 = liList2[0].offsetWidth;
    let newSize2 = (stateWidth2 - 3*liWidth2)/2 
    stateDiv2.style.setProperty('--width', `${newSize2}px`)
}
resize()
window.onresize = resize;

// Slider logic
const imageList1 = document.querySelectorAll('.slideshow1__image');
const imageList2 = document.querySelectorAll('.slideshow2__image');
imageList1.forEach((el) => {
    el.style.backgroundImage = `url('assets/${el.dataset.src}')`
})
imageList2.forEach((el) => {
    el.style.backgroundImage = `url('assets/${el.dataset.src}')`
})
class Slider {
    constructor(nbrState, iniState=1) {
        this.state=iniState
        this.nbrState=nbrState
    }
    increment () {
        if (this.state+1<=this.nbrState) {
            this.state++;
        }
    } 
    decrement () {
        if (this.state-1>0) {
            this.state--;
        }
    }  
}
const slider1 = new Slider(liList1.length)
const slider2 = new Slider(liList2.length)

const left1 = document.querySelector(".left1");
const right1 = document.querySelector(".right1");
const left2 = document.querySelector(".left2");
const right2 = document.querySelector(".right2");

left1.addEventListener("click", leftEvent1);
right1.addEventListener("click", rightEvent1);
left2.addEventListener("click", leftEvent2);
right2.addEventListener("click", rightEvent2);

function setState1 () {
    console.log(slider1.state)
    for (let i=0; i<slider1.nbrState; i++) {
        if (i<slider1.state) {
            liList1[i].classList.add("active")
        } else {
            liList1[i].classList.remove("active")
        }
        if (i+1==slider1.state) {
            imageList1[i].classList.add("in")  
            imageList1[i].classList.remove("out")  
        } else {
            imageList1[i].classList.add("out");
            imageList1[i].classList.remove("in");
        }
    }
    if (slider1.state==1) {
        left1.classList.add("stop")
    } else {
        left1.classList.remove("stop")
    }
    if (slider1.state==slider1.nbrState) {
        right1.classList.add("stop")
    } else {
        right1.classList.remove("stop")
    }
}
function setState2 () {
    console.log(slider2.state)
    for (let i=0; i<slider2.nbrState; i++) {
        if (i<slider2.state) {
            liList2[i].classList.add("active")
        } else {
            liList2[i].classList.remove("active")
        }
        if (i+1==slider2.state) {
            imageList2[i].classList.add("in")  
            imageList2[i].classList.remove("out")  
        } else {
            imageList2[i].classList.add("out");
            imageList2[i].classList.remove("in");
        }
    }
    if (slider2.state==1) {
        left2.classList.add("stop")
    } else {
        left2.classList.remove("stop")
    }
    if (slider2.state==slider2.nbrState) {
        right2.classList.add("stop")
    } else {
        right2.classList.remove("stop")
    }
}

function leftEvent1 () {
    slider1.decrement()
    setState1(slider1)
}

function rightEvent1 () {
    slider1.increment()
    setState1(slider1)
}

function leftEvent2 () {
    slider2.decrement()
    setState2(slider2)
}

function rightEvent2 () {
    slider2.increment()
    setState2(slider2)
}

function load () {
    resize();
    setState1(slider1);
    setState2(slider2);
}
window.onload = load;

// Shark selector

sharkList = document.querySelectorAll(".display__shark")
sharkList.forEach(el => {
    el.style.backgroundImage = `url("./assets/${el.dataset.src}")`
});
thumbList = document.querySelectorAll(".selector__choice__thumbnail")
thumbList.forEach(el => {
    el.style.backgroundImage = `url("./assets/${el.dataset.src}")`
});

choiceList = document.querySelectorAll(".selector__choice")
choiceList.forEach((el,index) => {
    el.addEventListener('click', () => {
        setShark(index);
        console.log(index, " trigger")
    })
    
})

function setShark (n) {
    for (let i=0; i<3; i++) {
        if (i==n) {
            choiceList[i].classList.add("active")
            sharkList[i].classList.add("active")
        } else {
            choiceList[i].classList.remove("active")
            sharkList[i].classList.remove("active")
        }
    }
}


// Scrollbar logic
const thumb = document.querySelector(".side__scrollbar__thumb")
const bg = document.querySelector(".side__scrollbar__bg")

function setThumbHeight () {
    let pageHeight = window.innerHeight;
    let documentHeight = document.body.scrollHeight;
    thumb.style.height = `${bg.offsetHeight}px`;
    console.log(thumb.style.height);
}